#include <iostream>

using namespace std;

class Element{
public:
    int data;
    Element* parent;
    Element* right;
    Element* left;
    int height;
    Element(int d){
        data=d;
        right=NULL;
        left=NULL;
        parent=NULL;
        height = 1;
    }
};

Element* root;

class Tree{
public:
    void insert (int d, Element* node);
    void in_order (Element* node);
    void rightrotate(Element *node);
    void leftrotate(Element *node);
    int height(Element* node);
    int nbBalance(Element* node);
    int max(int a, int b);
    Tree () {
        root=NULL;
    }
};

int Tree::max(int a, int b)
{
    return (a > b)? a : b;
}

void Tree::insert (int d, Element* node){
    if (root == NULL){
        root = new Element(d);
        return;
    }
    else if (d < node->data){
        if (node->left != NULL){
            insert(d, node->left);
        }
        else {
            Element* temp = new Element (d);
            temp->parent = node;
            node->left = temp;
        }
    }
    else {
        if (node->right != NULL){
            insert(d, node->right);
        }
        else {
            Element* temp = new Element (d);
            temp->parent = node;
            node->right = temp;
        }
    }

        node->height = 1 + max(height(node->left), height(node->right));
        int balance = nbBalance(node);
        cout<<"balance: "<<balance<<endl;

        cout << "node: " << node->data << " height: " << node->height<<endl;

        // Left Left Case
        if (balance > 1 && d < node->left->data) {
            cout<<"bnode1: "<<node->data<<endl;
            rightrotate(node);
            return;
        }

        // Right Right Case
        if (balance < -1 && d > node->right->data) {
            cout<<"bnode2: "<<node->data<<endl;
            leftrotate(node);
            return;
        }

        // Left Right Case
        if (balance > 1 && d > node->left->data) {
            cout<<"bnode3: "<<node->data<<endl;
            leftrotate(node->left);
            rightrotate(node);
            return;
        }

        // Right Left Case
        if (balance < -1 && d < node->right->data) {
            cout<<"bnode4: "<<node->data<<endl;
            rightrotate(node->right);
            leftrotate(node);
            return;
        }

    /*if (node != NULL){
        if (d< node->data){
            rightrotate(node);
            cout<<"po rotate node: "<<node->data;
        }
    }*/

}

int Tree::height(Element* node)     //zwroc wysokosc drzewa
{
    if (node == NULL)
        return 0;
    return node->height;
}

int Tree::nbBalance(Element* node)  //zwroc liczbe okreslajaca czy drzewo jest zbalanowane
{
    if (node == NULL)
        return 0;
    return height (node->left) - height(node->right);
}

void Tree::rightrotate(Element *node){
    int galaz = 0;      //zmienna przyjmuje wartosc 1 gdy rotowane poddrzewo jest prawym poddrzewem swojego rodzica
    //                         -1 gdy rotowane poddrzewo jest lewym poddrzewem swojego rodzica
    //                          0 gdy rotowane poddrzewo nie ma rodzica

    Element* temp = node;

    Element* x = temp;
    Element* xparent = x->parent;

    if (xparent) {
        if (xparent->right == x)
            galaz = 1;
        else if (xparent->left == x) galaz = -1;
    }
    Element* y = temp->left;
    Element* yp = y->right;
    y->right = x;
    x->left = yp;

    y->parent = xparent;

    if (galaz == 1) xparent->right = y;
    else if (galaz == -1) xparent->left = y;

    x->parent = y;
    x->left = yp;
    if (yp){
        yp->parent = x;
    }
    if (y->parent == NULL){
        root = y;
    }
    x->height = 1+ max(height(x->left), height(x->right));
    y->height = 1+ max(height(y->left), height(y->right));

}

void Tree::leftrotate(Element *node) {
    int galaz = 0;      //zmienna przyjmuje wartosc 1 gdy rotowane poddrzewo jest prawym poddrzewem swojego rodzica
    //                         -1 gdy rotowane poddrzewo jest lewym poddrzewem swojego rodzica
    //                          0 gdy rotowane poddrzewo nie ma rodzica

    Element* temp = node;

    Element* x = temp;
    Element* xparent = x->parent;

    if (xparent) {
        if (xparent->right == x)
            galaz = 1;
        else if (xparent->left == x) galaz = -1;
    }
    Element* y = temp->right;
    Element* yl = y->left;
    y->left = x;
    x->right = yl;

    y->parent = xparent;

    if (galaz == 1) xparent->right = y;
    else if (galaz == -1) xparent->left = y;

    x->parent = y;
    x->right = yl;
    if (yl){
        yl->parent = x;
    }
    if (y->parent == NULL){
        root = y;
    }
    x->height = 1 + max(height(x->left), height(x->right));
    y->height = 1 + max(height(y->left), height(y->right));
}

void Tree::in_order (Element* node){
    if(node->left != NULL)
        in_order (node->left);

    cout<<endl<<node->data;
    if (node->left != NULL) cout<<" Lewy wierzcholek: "<<node->left->data;
    if (node->right != NULL) cout<<" Prawy wierzcholek: "<<node->right->data;

    if(node->right != NULL)
        in_order (node->right);
}

int main() {
    Tree *tree = new Tree;
    //dodanie elemntow do drzewa
    tree->insert(10, root);
    tree->insert(13, root);
    tree->insert(16, root);
    tree->insert(14, root);
    tree->insert(15, root);
    tree->in_order(root);


    return 0;
}